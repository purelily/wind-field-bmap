import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BMapComponent } from './b-map/b-map.component';


const routes: Routes = [
  {
    path: '',
    component: BMapComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BMapRoutingModule { }

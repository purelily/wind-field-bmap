import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BMapRoutingModule } from './b-map-routing.module';
import { BMapComponent } from './b-map/b-map.component';
import { BMapService } from './b-map.service';
import { FormsModule } from '@angular/forms';
import { DashboardModule } from '../dashboard/dashboard.module';
import { DashboardService } from '../dashboard/dashboard.service';
import { WindFieldComponent } from './component/wind-field/wind-field.component';


@NgModule({
  declarations: [
    BMapComponent,
    WindFieldComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    BMapRoutingModule
  ],
  providers: [
    BMapService,
    DashboardService,
  ]
})
export class BMapModule { }

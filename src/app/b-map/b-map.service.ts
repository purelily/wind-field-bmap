import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class BMapService {

  header = new HttpHeaders();

  constructor(
    private http: HttpClient
  ) {
    this.header = this.header.set(
      'Cookie', 'isRemember=true; _ga=GA1.1.1235107416.1614649574; userName=admin; isLogin=true; userId=1; nickName=%E8%B6%85%E7%BA%A7%E7%AE%A1%E7%90%86%E5%91%98; userType=0'
    );
  }

  getHeatMapData(options: {
    left?: number,
    right?: number,
    top?: number,
    bottom?: number,
    type?: string,
    emissionStandard?: number,
    inProvince?: number,
    vehicleCategory?: number,
    time?: number,
  }): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Cookie', 'isRemember=true; _ga=GA1.1.1235107416.1614649574; userName=admin; Hm_lvt_e8002ef3d9e0d8274b5b74cc4a027d08=1622101899,1622165089,1622774404,1623045721; Hm_lpvt_e8002ef3d9e0d8274b5b74cc4a027d08=1623047233; isLogin=true; userId=1; nickName=%E8%B6%85%E7%BA%A7%E7%AE%A1%E7%90%86%E5%91%98; userType=0');
    let httpParams = new HttpParams();
    httpParams = options.left ? httpParams.set('left', options.left + '') : httpParams;
    httpParams = options.right ? httpParams.set('right', options.right + '') : httpParams;
    httpParams = options.top ? httpParams.set('top', options.top + '') : httpParams;
    httpParams = options.bottom ? httpParams.set('bottom', options.bottom + '') : httpParams;
    httpParams = options.type ? httpParams.set('type', options.type + '') : httpParams;
    httpParams = options.emissionStandard ? httpParams.set('emissionStandard', options.emissionStandard + '') : httpParams;
    httpParams = options.inProvince ? httpParams.set('inProvince', options.inProvince + '') : httpParams;
    httpParams = options.vehicleCategory ? httpParams.set('vehicleCategory', options.vehicleCategory + '') : httpParams;
    httpParams = options.time ? httpParams.set('time', options.time + '') : httpParams;
    return this.http.get(`../../assets/json/heat1.json`);
    // return this.http.get<any>(`${environment.BASE_URL}/locate/gps-data`, {
    //   headers,
    //   params: httpParams
    // });
  }

  getMapStyleJson(): Observable<any> {
    return this.http.get<any>('../../assets/json/map-style.json');
  }
}


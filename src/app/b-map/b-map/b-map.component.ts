import { Component, OnInit } from '@angular/core';
import { CoordinateConversionService } from 'src/app/shared/coordinate-conversion.service';
import { BMapService } from '../b-map.service';
import { MapOptionObj } from '../component/wind-field/wind-field.component';
// import * as mapvgl from 'mapvgl';

declare var BMap: any;
declare var BMapGL: any;
declare var BMapLib: any;
declare var mapvgl: any;

@Component({
  selector: 'app-b-map',
  templateUrl: './b-map.component.html',
  styleUrls: ['./b-map.component.less']
})
export class BMapComponent implements OnInit {

  map: any;

  heatmapOverlay: any;

  view: any;

  styleJson: any;

  mapOptions: MapOptionObj = new MapOptionObj();

  constructor(
    private bmapService: BMapService,
    private coordinateConversionService: CoordinateConversionService
  ) { }

  ngOnInit(): void {
    // this.initMap();
    this.getMapStyleJson();
  }

  getMapStyleJson() {
    this.bmapService.getMapStyleJson()
      .subscribe(res => {
        this.styleJson = res;
        this.initMap();
      });
  }

  initMap() {
    this.map = new BMap.Map('map');
    const point = new BMap.Point(121.687899486, 31.10916171); // 创建点坐标
    this.map.centerAndZoom(point, 11); // 初始化地图，设置中心点坐标和地图级别
    this.map.enableScrollWheelZoom(true); // 开启鼠标滚轮缩放
    this.map.setMapStyleV2({
      styleId: 'bad330543be59326c2030d727ab8a86b'
      // styleJson: this.styleJson
    });
    this.mapOptions = {
      center: [point.lng, point.lat],
      zoom: 11
    };
    // this.view = new mapvgl.View({
    //   map: this.map
    // });
    const change = () => {
      const center = this.map.getCenter();
      const zoom = this.map.getZoom();
      this.mapOptions = {
        center: [center.lng, center.lat],
        zoom
      };
    };

    this.map.addEventListener('movestart', change);
    this.map.addEventListener('moving', change);
    this.map.addEventListener('moveend', change);
    this.map.addEventListener('zoomstart', change);
    this.map.addEventListener('zoomend', change);
    // this.getHeatMapData();
    this.setMarker();
  }

  // initialize() {
  //   debugger
  //   this.map = new BMap.map('map');
  //   const point = new BMap.Point(121.687899486, 31.10916171); // 创建点坐标
  //   this.map.centerAndZoom(point, 10); // 初始化地图，设置中心点坐标和地图级别
  //   this.map.enableScrollWheelZoom(true); // 开启鼠标滚轮缩放
  //   this.map.setMapStyleV2({
  //     styleId: 'f7e0aa99e2b84ac6e67ab8049a92bc2e'
  //   });
  // }

  // loadScript() {
  //   const script = document.createElement('script');
  //   script.src = 'http://api.map.baidu.com/api?v=3.0&ak=tKTpGlsUa18NwryzGAmlTfByXx6W5emb&callback=initialize';
  //   document.body.appendChild(script);
  //   debugger
  // }

  getHeatMapData() {
    const left = this.map.getBounds().getSouthWest().lng;
    const bottom = this.map.getBounds().getSouthWest().lat;
    const right = this.map.getBounds().getNorthEast().lng;
    const top = this.map.getBounds().getNorthEast().lat;
    const type = 'traffic';

    this.bmapService.getHeatMapData({
      left,
      right,
      top,
      bottom,
      type
    }).subscribe(res => {
      this.setHeatMap(res);
    });
  }

  isSupportHeatMap() {
    if (!this.isSupportCanvas()) {
      alert('热力图目前只支持有canvas支持的浏览器,您所使用的浏览器不能使用热力图功能~');
      return;
    }
  }

  // 详细的参数,可以查看heatmap.js的文档 https://github.com/pa7/heatmap.js/blob/master/README.md
  // 参数说明如下:
  /* visible 热力图是否显示,默认为true
   * opacity 热力的透明度,1-100
   * radius 势力图的每个点的半径大小
   * gradient  {JSON} 热力图的渐变区间 . gradient如下所示
   *  {
          .2:'rgb(0, 255, 255)',
          .5:'rgb(0, 110, 255)',
          .8:'rgb(100, 0, 255)'
      }
      其中 key 表示插值的位置, 0~1.
          value 为颜色值.
   */
  setHeatMap(points: any[]) {
    points = points.filter(point => point.count);
    points.forEach(point => {
      const value = this.coordinateConversionService.gd2bd(point.lng, point.lat);
      point.lng = value.lng;
      point.lat = value.lat;
    });
    this.isSupportHeatMap();
    const gradient = {
      // hky
      0.2: '#339933',
      0.4: '#669933',
      0.6: '#CCCC33',
      0.8: '#FF9966',
      1.0: '#CC0033'
    };
    this.heatmapOverlay = new BMapLib.HeatmapOverlay({ radius: 6, visible: true, gradient, opacity: 0.65 });
    this.map.addOverlay(this.heatmapOverlay);
    this.heatmapOverlay.setDataSet({ data: points, max: 310 });

    // const data = [];
    // for (const item of points) {
    //   data.push({
    //     geometry: {
    //       type: 'Point',
    //       coordinates: [item.lng, item.lat]
    //     },
    //     properties: {
    //       count: item.count
    //     }
    //   });
    // }

    // const heatmap = new mapvgl.HeatmapLayer({
    //   size: 1500, // 单个点绘制大小
    //   max: 2000, // 最大阈值
    //   height: 0, // 最大高度，默认为0
    //   unit: 'm', // 单位，m:米，px: 像素
    //   gradient: { // 对应比例渐变色
    //     0.2: '#339933',
    //     0.4: '#669933',
    //     0.6: '#CCCC33',
    //     0.8: '#FF9966',
    //     1.0: '#CC0033'
    //     // 0.25: '#339933',
    //     // 0.55: '#669933',
    //     // 0.85: '#FF9966',
    //     // 1.0: '#CC0033'
    //     // 0.25: 'rgba(0, 0, 255, 1)',
    //     // 0.55: 'rgba(0, 255, 0, 1)',
    //     // 0.85: 'rgba(255, 255, 0, 1)',
    //     // 1: 'rgba(255, 0, 0, 1)'
    //   }
    // });
    // this.view.addLayer(heatmap);
    // heatmap.setData(data);
  }

  // closeHeatmap();



  // 判断浏览区是否支持canvas
  isSupportCanvas() {
    const elem = document.createElement('canvas');
    return !!(elem.getContext && elem.getContext('2d'));
  }

  setGradient() {
    /*格式如下所示:
    {
        0:'rgb(102, 255, 0)',
        .5:'rgb(255, 170, 0)',
        1:'rgb(255, 0, 0)'
    }*/
    const gradient = {};
    // let colors = document.querySelectorAll("input[type='color']");
    // colors = [].slice.call(colors, 0);
    // colors.forEach((ele){
    //   gradient[ele.getAttribute("data-key")] = ele.value;
    // });
    // this.heatmapOverlay.setOptions({ "gradient": gradient });
  }

  openHeatmap() {
    this.heatmapOverlay.show();
  }

  closeHeatmap() {
    this.heatmapOverlay.hide();
  }

  setMarker() {
    const myIcon = new BMap.Icon('/assets/image/station.png', new BMap.Size(40, 40));
    // 创建Marker标注，使用小车图标
    const point = this.map.getCenter();
    const marker = new BMap.Marker(point, {
      icon: myIcon
    });
    // 将标注添加到地图
    this.map.addOverlay(marker);
  }
}

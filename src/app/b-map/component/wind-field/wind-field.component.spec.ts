import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WindFieldComponent } from './wind-field.component';

describe('WindFieldComponent', () => {
  let component: WindFieldComponent;
  let fixture: ComponentFixture<WindFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WindFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WindFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

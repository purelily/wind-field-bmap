import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import * as echarts from 'echarts';
import { Subscription, fromEvent } from 'rxjs';
import { DashboardService } from 'src/app/dashboard/dashboard.service';

import 'echarts-gl';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';
import 'echarts/extension/bmap/bmap';
import 'echarts/lib/component/visualMap';

declare var BMap: any;

@Component({
  selector: 'app-wind-field',
  templateUrl: './wind-field.component.html',
  styleUrls: ['./wind-field.component.less']
})
export class WindFieldComponent implements OnInit {

  sub$ = new Subscription();

  @ViewChild('windEle') windEle: ElementRef;

  windChart: any;

  mapCenter = [121.687899486, 31.10916171];

  mapZoom = 11;

  _options: MapOptionObj;

  @Input()
  private set options(_value: MapOptionObj) {
    this.mapCenter = _value.center;
    this.mapZoom = _value.zoom;
    if (this.map) {
      const point = new BMap.Point(this.mapCenter[0], this.mapCenter[1]);
      this.map.setCenter(point);
      this.map.setZoom(this.mapZoom);
    } else {
      this.getWindFieldData();
    }
    this._options = _value;
  }

  private get options(): MapOptionObj {
    return this._options;
  }

  mapStyle = 'amap://styles/8253096644218eade45ccb432ab8a4e6';

  map: any;

  constructor(
    private dashboardService: DashboardService,
  ) { }

  ngOnInit(): void {
    // this.getWindFieldData();
    this.sub$ = fromEvent(window, 'resize').subscribe(_ => this.windChart && this.windChart.resize());
  }

  /**
   * 获取数据
   */
  getWindFieldData() {
    this.dashboardService.getWindFieldData('20210521')
      .subscribe(res => {
        this.buildGrid(res, (header, grid) => {
          const data = [];
          let maxMag = 0;
          let minMag = Infinity;
          for (let j = 0; j < header.ny; j++) {
            for (let i = 0; i < header.nx; i++) {
              const vx = grid[j][i][0];
              const vy = grid[j][i][1];
              const mag = Math.sqrt(vx * vx + vy * vy);
              let lng = i / header.nx * 360;
              if (lng >= 180) {
                lng = 180 - lng;
              }
              // const lngLat = this.bd2gd({ lng, lat: 90 - j / header.ny * 180 });
              // // 数据是一个一维数组
              // // [ [经度, 纬度，向量经度方向的值，向量纬度方向的值] ]
              // data.push([
              //   lngLat.lng,
              //   lngLat.lat,
              //   vx,
              //   vy,
              //   mag
              // ]);
              // maxMag = Math.max(mag, maxMag);
              // minMag = Math.min(mag, minMag);
              // 数据是一个一维数组
              // [ [经度, 纬度，向量经度方向的值，向量纬度方向的值] ]
              data.push([
                lng,
                90 - j / header.ny * 180,
                vx,
                vy,
                mag
              ]);
              maxMag = Math.max(mag, maxMag);
              minMag = Math.min(mag, minMag);
            }
          }
          if (!this.windChart) {
            this.windChart = echarts.init(this.windEle.nativeElement);
            const option = this.createEchartOptions({ data, minMag, maxMag });
            this.windChart.setOption(option, true);
            this.map = this.windChart.getModel().getComponent('bmap').getBMap();
            this.map.setMapStyleV2({
              styleId: 'bad330543be59326c2030d727ab8a86b'
            });
          }
        });
      });
  }

  createWindBuilder = (uComp, vComp) => {
    const uData = uComp.data;
    const vData = vComp.data;
    return {
      header: uComp.header,
      data: (i) => {
        return [uData[i], vData[i]];
      }
    };
  }

  createBuilder = (data) => {
    let uComp = null;
    let vComp = null;
    let scalar = null;

    data.forEach((record) => {
      switch (record.header.parameterCategory + ',' + record.header.parameterNumber) {
        case '2,2':
          uComp = record;
          break;
        case '2,3':
          vComp = record;
          break;
        default:
          scalar = record;
      }
    });

    return this.createWindBuilder(uComp, vComp);
  }

  buildGrid = (data, callback) => {
    const builder = this.createBuilder(data);

    const header = builder.header;
    const λ0 = header.lo1;
    const φ0 = header.la1; // the grid's origin (e.g., 0.0E, 90.0N)
    const Δλ = header.dx;
    const Δφ = header.dy; // distance between grid points (e.g., 2.5 deg lon, 2.5 deg lat)
    const ni = header.nx;
    const nj = header.ny; // number of grid points W-E and N-S (e.g., 144 x 73)
    const date = new Date(header.refTime);
    date.setHours(date.getHours() + header.forecastTime);

    const grid = [];
    let p = 0;
    const isContinuous = Math.floor(ni * Δλ) >= 360;
    for (let j = 0; j < nj; j++) {
      const row = [];
      for (let i = 0; i < ni; i++, p++) {
        row[i] = builder.data(p);
      }
      if (isContinuous) {
        row.push(row[0]);
      }
      grid[j] = row;
    }
    callback(header, grid);
  }

  createEchartOptions(metaData: { data: any[], minMag: number, maxMag: number }): any {
    const option = {

      visualMap: {
        left: 'right',
        min: metaData.minMag,
        max: metaData.maxMag,
        dimension: 4,
        inRange: {
          // color: ['green', 'yellow', 'red']
          color: ['#313695', '#4575b4', '#74add1', '#abd9e9', '#e0f3f8', '#ffffbf', '#fee090', '#fdae61', '#f46d43', '#d73027', '#a50026']
        },
        realtime: false,
        calculable: true,
        textStyle: {
          color: '#fff'
        }
      },
      bmap: {
        center: this.mapCenter,
        zoom: this.mapZoom,
        roam: true,
      },
      series: [{
        type: 'flowGL',
        coordinateSystem: 'bmap',
        data: metaData.data,
        supersampling: 4,
        particleType: 'line',
        particleDensity: 128,
        particleSpeed: 1,
        itemStyle: {
          opacity: 0.7
        }
      }]
    };

    return option;
  }
}

export class MapOptionObj {
  zoom: number;
  center: number[];
}


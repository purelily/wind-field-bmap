import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class DashboardService {

  constructor(
    private http: HttpClient
  ) { }

  getWindFieldData(dateTime: string): Observable<any> {
    return this.http.get(`${environment.WIND_URL}/json/${dateTime}`);
    // return this.http.get('/assets/json/demo1.json');
  }
}

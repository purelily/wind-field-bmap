import { TestBed } from '@angular/core/testing';

import { CoordinateConversionService } from './coordinate-conversion.service';

describe('CoordinateConversionService', () => {
  let service: CoordinateConversionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CoordinateConversionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

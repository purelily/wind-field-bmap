import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CoordinateConversionService {
  PI = Math.PI;
  a = 6378245.0;
  ee = 0.00669342162296594323;
  X_PI = this.PI * 3000.0 / 180.0;
  constructor() { }
  /**
   * wgs84坐标转百度坐标
   * @param lat 经度
   * @param lng 纬度
   */
  wgs2bd(lat, lng) {
    const wgs2gcj = this.wgs2gcj(lat, lng);
    const gcj2bd = this.gcj2bd(wgs2gcj[0], wgs2gcj[1]);
    return gcj2bd;
  }
  gcj2bd(lat, lng) {
    const x = lng;
    const y = lat;
    const z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * this.X_PI);
    const theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * this.X_PI);
    const bdLon = z * Math.cos(theta) + 0.0065;
    const bdLat = z * Math.sin(theta) + 0.006;
    return [bdLat, bdLon];
  }
  bd2gcj(lat, lng) {
    const x = lng - 0.0065;
    const y = lat - 0.006;
    const z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * this.X_PI);
    const theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * this.X_PI);
    const ggLon = z * Math.cos(theta);
    const ggLat = z * Math.sin(theta);
    return [ggLat, ggLon];
  }
  wgs2gcj(lat, lng) {
    let dLat = this.transformLat(lng - 105.0, lat - 35.0);
    let dLon = this.transformLng(lng - 105.0, lat - 35.0);
    const radLat = lat / 180.0 * this.PI;
    let magic = Math.sin(radLat);
    magic = 1 - this.ee * magic * magic;
    const sqrtMagic = Math.sqrt(magic);
    dLat = (dLat * 180.0) / ((this.a * (1 - this.ee)) / (magic * sqrtMagic) * this.PI);
    dLon = (dLon * 180.0) / (this.a / sqrtMagic * Math.cos(radLat) * this.PI);
    const mgLat = lat + dLat;
    const mgLon = lng + dLon;
    return [mgLat, mgLon];
  }
  /**
   * 坐标转换wgs84转高德
   * @param lngLat 转换前坐标
   * @returns 转换后坐标
   */
  wgs2gd(lngLat: { lng: number; lat: number }): { lng: number; lat: number } {
    const lng = lngLat.lng;
    const lat = lngLat.lat;
    if (this.out_of_china(lng, lat)) {
      return { lng, lat };
    }
    else {
      let dlat = this.transformLat(lng - 105.0, lat - 35.0);
      let dlng = this.transformLng(lng - 105.0, lat - 35.0);
      const radlat = lat / 180.0 * this.PI;
      let magic = Math.sin(radlat);
      magic = 1 - this.ee * magic * magic;
      const sqrtmagic = Math.sqrt(magic);
      dlat = (dlat * 180.0) / ((this.a * (1 - this.ee)) / (magic * sqrtmagic) * this.PI);
      dlng = (dlng * 180.0) / (this.a / sqrtmagic * Math.cos(radlat) * this.PI);
      const mglat = lat + dlat;
      const mglng = lng + dlng;
      return {
        lng: mglng,
        lat: mglat
      };
    }
  }
  /**
   * 百度坐标转高德（传入经度、纬度）
   * @param lng 经度
   * @param lat 纬度
   */
  bd2gd(lng, lat) {
    const x = lng - 0.0065;
    const y = lat - 0.006;
    const z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * this.X_PI);
    const theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * this.X_PI);
    lng = z * Math.cos(theta);
    lat = z * Math.sin(theta);
    return {
      lng,
      lat
    };
  }
  /**
   * 高德坐标转百度（传入经度、纬度）
   * @param lng 经度
   * @param lat 纬度
   */
  gd2bd(lng, lat) {
    const x = lng;
    const y = lat;
    const z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * this.X_PI);
    const theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * this.X_PI);
    lng = z * Math.cos(theta) + 0.0065;
    lat = z * Math.sin(theta) + 0.006;
    return {
      lng,
      lat
    };
  }
  transformLat(lng, lat) {
    let ret = -100.0 + 2.0 * lng + 3.0 * lat + 0.2 * lat * lat + 0.1 * lng * lat + 0.2 * Math.sqrt(Math.abs(lng));
    ret += (20.0 * Math.sin(6.0 * lng * this.PI) + 20.0 * Math.sin(2.0 * lng * this.PI)) * 2.0 / 3.0;
    ret += (20.0 * Math.sin(lat * this.PI) + 40.0 * Math.sin(lat / 3.0 * this.PI)) * 2.0 / 3.0;
    ret += (160.0 * Math.sin(lat / 12.0 * this.PI) + 320 * Math.sin(lat * this.PI / 30.0)) * 2.0 / 3.0;
    return ret;
  }
  transformLng(lng, lat) {
    let ret = 300.0 + lng + 2.0 * lat + 0.1 * lng * lng + 0.1 * lng * lat + 0.1 * Math.sqrt(Math.abs(lng));
    ret += (20.0 * Math.sin(6.0 * lng * this.PI) + 20.0 * Math.sin(2.0 * lng * this.PI)) * 2.0 / 3.0;
    ret += (20.0 * Math.sin(lng * this.PI) + 40.0 * Math.sin(lng / 3.0 * this.PI)) * 2.0 / 3.0;
    ret += (150.0 * Math.sin(lng / 12.0 * this.PI) + 300.0 * Math.sin(lng / 30.0 * this.PI)) * 2.0 / 3.0;
    return ret;
  }
  /**
   * 判断是否在国内，不在国内则不做偏移
   */
  out_of_china(lng, lat) {
    return (lng < 72.004 || lng > 137.8347) || ((lat < 0.8293 || lat > 55.8271) || false);
  }
}
